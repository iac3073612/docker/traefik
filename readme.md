# Init config

Get static confiuration of traefik from official docs, and if needed dynamic confiuration from Templates section.

# Spin up

```bash
docker compose --env-file .env.local up -d
```

# Spin down

```bash
docker compose --env-file .env.local down
```

# Templates

### Dynamic config

```file-provider.yml
http:
  routers:
    service-name:
      entryPoints:
        - websecure
      rule: "Host(`host-header`)"
      tls: {}
      service: service-name

  services:
    service-name:
      loadBalancer:
        servers:
          - url: "scheme://backend-address:port"
```
